<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }
    
    public function send(Request $request){
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];

        return view('page.home', ['namaDepan' => $namaDepan , 'namaBelakang' => $namaBelakang]);
    }
}
