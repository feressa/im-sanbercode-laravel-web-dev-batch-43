<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class , 'utama']);
Route::get('/register', [AuthController::class , 'register']);

Route::post('/welcome', [AuthController::class , 'send']);

// template
Route::get('/table', function() {
    return view('page.table');
});

Route::get('/data-table', function() {
    return view('page.data-table');
});

//CRUD cast
//create data
//input cast
Route::get('/cast/create', [CastController::class , 'create']);
//memasukkan ke database
Route::post('/cast', [CastController::class , 'store']);
//menampilkan data yg ada di db
Route::get('/cast', [CastController::class , 'index']);
Route::get('/cast/{id}', [CastController::class , 'show']);
//update
Route::get('/cast/{cast_id}/edit', [CastController::class , 'edit']);
Route::put('/cast/{cast_id}', [CastController::class , 'update']);
//delete
Route::delete('/cast/{cast_id}', [CastController::class , 'destroy']);