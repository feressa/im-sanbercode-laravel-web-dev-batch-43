@extends('layouts.master')
@section('title')
    Halaman Sign Up
@endsection
@section('sub-title')
    New Account
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/welcome" method="post">
    @csrf
    <label>First Name:</label><br><br>
    <input type="text" name="firstname"><br><br>
    <label>Last Name:</label><br><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender" value="1">Male<br>
    <input type="radio" name="gender" value="2">Female<br>
    <input type="radio" name="gender" value="3">Other<br><br>
    <label for="nationality">Nationality:</label><br><br>
        <select name="Indonesian">
            <option value="1">Indonesian</option><br>
            <option value="2">Foreign Nationals</option>
        </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" value="1" name="skill">Bahasa Indonesian<br>
    <input type="checkbox" value="2" name="skill">English<br>
    <input type="checkbox" value="3" name="skill">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="message" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" name="submit" value="Sign Up" /> 
</form>
@endsection