@extends('layouts.master')
@section('title')
    Halaman Beranda
@endsection
@section('sub-title')
    Selamat anda telah berhasil sign up!
@endsection
@section('content')
    <h1>SELAMAT DATANG! {{$namaDepan}} {{$namaBelakang}}</h1>
    <h2>Terima Kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection